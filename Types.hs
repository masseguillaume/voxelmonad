{-# LANGUAGE Arrows #-}
module Types where

import FRP.Yampa
import FRP.Yampa.Vector3

import Graphics.UI.GLUT hiding (Level,Vector3(..),normalize)
import qualified Graphics.UI.GLUT as G(Vector3(..))

cubeSize :: GLdouble
cubeSize = 2

data Input = Keyboard { key       :: Key
                      , keyState  :: KeyState
                      , modifiers :: Modifiers }

data Ball = Ball { bY :: GLdouble }

data GameState = GameState { ball :: Ball
                           , rotX :: GLdouble
                           , rotY :: GLdouble }

data ParsedInput =
    ParsedInput { wCount    :: Double
                , aCount    :: Double
                , sCount    :: Double
                , dCount    :: Double
                , upEvs     :: Event Input
                , downEvs   :: Event Input
                , rightEvs  :: Event Input
                , leftEvs   :: Event Input }