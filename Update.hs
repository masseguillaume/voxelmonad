{-# LANGUAGE Arrows, BangPatterns, NamedFieldPuns #-}
{-# OPTIONS_GHC -XUnicodeSyntax #-}

module Update (update) where

import Prelude.Unicode

import FRP.Yampa
import FRP.Yampa.Vector3
import FRP.Yampa.Utilities
import Graphics.UI.GLUT hiding (Level,Vector3(..),normalize)
import qualified Graphics.UI.GLUT as G(Vector3(..))

import Types
import Input

type Force = Double
type Rotation = Double
type Moment = Double

data Collision = Collision

limit = π/2

rotation :: SF () (Rotation, Event Collision)
rotation = proc () → do
  rec
    ω ← integral -< (10 - cos(θ - π/2))
    θ ← integral -< ω
    hit ← edge -< θ >= limit
  returnA -< (θ, hit `tag` Collision)

clampRotation :: SF () Rotation
clampRotation = switch rotation $ \_ -> constant limit

update :: SF ParsedInput GameState
update = proc ParsedInput{ wCount, aCount, sCount, dCount } → do
  θ ← clampRotation -< ()
  returnA -< GameState { ball = Ball (realToFrac θ)
                       , rotX = realToFrac (wCount - sCount)
                       , rotY = 0 }
