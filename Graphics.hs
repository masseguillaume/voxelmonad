module Graphics (initGL, draw) where

import Prelude.Unicode

import FRP.Yampa
import FRP.Yampa.Vector3
import FRP.Yampa.Utilities

import Graphics.UI.GLUT hiding (Level,Vector3(..),normalize)
import qualified Graphics.UI.GLUT as G(Vector3(..))

import Types

initGL :: IO ()
initGL = do
    getArgsAndInitialize
    createWindow "Cuboid!"
    initialDisplayMode $= [ WithDepthBuffer ]
    depthFunc          $= Just Less
    clearColor         $= Color4 1 1 1 1
    light (Light 0)    $= Enabled
    lighting           $= Enabled
    lightModelAmbient  $= Color4 0.5 0.5 0.5 1
    diffuse (Light 0)  $= Color4 0.5 0 0 1
    blend              $= Enabled
    blendFunc          $= (SrcAlpha, OneMinusSrcAlpha)
    colorMaterial      $= Just (FrontAndBack, AmbientAndDiffuse)
    reshapeCallback    $= Just resizeScene
    return ()

-- Copied from reactive-glut
resizeScene :: Size -> IO ()
resizeScene (Size w 0) = resizeScene (Size w 1) -- prevent divide by zero
resizeScene s@(Size width height) = do
  viewport   $= (Position 0 0, s)
  matrixMode $= Projection
  loadIdentity
  perspective 45 (w2/h2) 1 1000
  matrixMode $= Modelview 0
  flush
 where
   w2 = half width
   h2 = half height
   half z = realToFrac z / 2

-- Rendering Code:
xAxis = G.Vector3 1 0 0 :: G.Vector3 GLdouble
zAxis = G.Vector3 0 0 1 :: G.Vector3 GLdouble

renderGame :: GameState -> IO ()
renderGame (GameState (Ball θ) _ _) = do
  loadIdentity
  color $ (Color3 0.4 0.4 0.4 :: Color3 GLdouble)
  position (Light 0) $= Vertex4 3 2 4 1
  translate $ (G.Vector3 0 0 (-10) :: G.Vector3 GLdouble)
  rotate 25 xAxis
  preservingMatrix $ do
    rotate (θ * 180 / π) zAxis
    translate $ (G.Vector3 (cubeSize/2) (cubeSize/2) 0 :: G.Vector3 GLdouble)
    renderObject Solid $ Cube cubeSize
  flush

draw :: SF GameState (IO ())
draw = arr $ (\gs -> do
        clear [ ColorBuffer, DepthBuffer ]
        renderGame gs
        flush)

