{-# LANGUAGE Arrows #-}
module Input (parseInput) where

import FRP.Yampa
import FRP.Yampa.Utilities

import Graphics.UI.GLUT

import Types

-- Event Definition:
filterKeyDowns :: SF (Event Input) (Event Input)
filterKeyDowns = arr $ filterE ((==Down) . keyState)

keyIntegral :: Double -> SF (Event a) Double
keyIntegral a = let eventToSpeed (Event _) = a
                    eventToSpeed NoEvent   = 0 
                in arr eventToSpeed >>> integral 
                       
-- Input
parseInput :: SF (Event Input) ParsedInput
parseInput = proc i -> do
    down      <- filterKeyDowns                  -< i
    _wCount   <- countKey 'w'                    -< down
    _aCount   <- countKey 'a'                    -< down
    _sCount   <- countKey 's'                    -< down
    _dCount   <- countKey 'd'                    -< down
    _upEvs    <- filterKey (SpecialKey KeyUp)    -< down
    _downEvs  <- filterKey (SpecialKey KeyDown)  -< down
    _rightEvs <- filterKey (SpecialKey KeyRight) -< down
    _leftEvs  <- filterKey (SpecialKey KeyLeft)  -< down
    returnA   -< ParsedInput _wCount _aCount _sCount _dCount 
                           _upEvs _downEvs _rightEvs _leftEvs
    where countKey c  = filterE ((==(Char c)) . key) ^>> keyIntegral 1
          filterKey k = arr $ filterE ((==k) . key)


